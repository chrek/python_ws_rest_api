import pyodbc

# connect to API Service Using ZappySys ODBC driver for JSON

# Use DSN
conn = pyodbc.connect('DSN=MyZappySysJsonDSN;')

# OR Use Direct Connection String
# conn = pyodbc.connect(
#     r'DRIVER={ZappySys JSON Driver};'
# )
cursor = conn.cursor()

# execute query to fetch data from API service
cursor.execute("SELECT * FROM value ORDER BY Country WITH (SRC='https://services.odata.org/V3/Northwind/Northwind.svc/Invoices?$format=json')")
# cursor.execute("SELECT CUSTOMERID, CUSTOMERNAME FROM value ORDER BY Country WITH (SRC='https://services.odata.org/V3/Northwind/Northwind.svc/Invoices?$format=json')")
# cursor.execute("SELECT ORDERID,CustomerID FROM value WHERE SHIPCOUNTRY='Germany' WITH (SRC='https://services.odata.org/V3/Northwind/Northwind.svc/Orders?$format=json')")

row = cursor.fetchone()
while row:
    print(row[2])
    row = cursor.fetchone()

