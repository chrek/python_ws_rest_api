# ====================================
# Get values from Multiple JSON Files
# ====================================

import pyodbc

conn = pyodbc.connect(
    r'DRIVER={ZappySys JSON Driver};'
)
cursor = conn.cursor()

# execute query to fetch data from API service
# cursor.execute("SELECT CustomerID, ContactName FROM value WITH (SRC=@'C:/sql/cust*.json')")
cursor.execute("SELECT CustomerID, ContactName FROM value WITH (SRC=@'data/cust*.json')")

row = cursor.fetchone()
while row:
    # print two rows
    print(row[0], row[1])
    row = cursor.fetchone()
