import csv
import pyodbc

# conn = pyodbc.connect(
#     r'DRIVER={ZappySys JSON Driver};'
# )

# Use DSN
conn = pyodbc.connect(r'DSN=MyZappySysJsonDSN;')


cursor = conn.cursor()
rows = cursor.execute("SELECT ORDERID,CustomerID FROM value WHERE SHIPCOUNTRY='Germany' WITH (SRC='https://services.odata.org/V3/Northwind/Northwind.svc/Orders?$format=json')")
# rows = cursor.execute("SELECT CustomerID,CompanyName FROM value WHERE COUNTRY='Germany' WITH (SRC='https://services.odata.org/V3/Northwind/Northwind.svc/Customers?$format=json')")
with open(r'C:\sql\cus2.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow([x[0] for x in cursor.description])
    for row in rows:
        writer.writerow(row)