# ==============================================
# Store data from REST API or JSON to a CSV file
# ==============================================
import csv
import pyodbc

conn = pyodbc.connect(
    r'DRIVER={ZappySys JSON Driver};'
)
cursor = conn.cursor()
url = 'https://services.odata.org/V3/Northwind/Northwind.svc/Customers?$format=json'
# execute query to fetch data from API service
rows = cursor.execute("SELECT CustomerID,CompanyName FROM value WHERE Country='Germany' WITH (SRC='https://services.odata.org/V3/Northwind/Northwind.svc/Customers?$format=json')")

with open(r'C:\sql\customer.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow([x[0] for x in cursor.description])

    for row in rows:
        writer.writerow(row)
