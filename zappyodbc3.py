# =====================================================
# Get values from REST API and JSON with a where clause
# =====================================================

import pyodbc

# Use DSN
conn = pyodbc.connect('DSN=MyZappySysJsonDSN;')

# OR Use Direct Connection String
# conn = pyodbc.connect(
#     r'DRIVER={ZappySys JSON Driver};'
# )
cursor = conn.cursor()

# execute query to fetch data from API service
cursor.execute("SELECT CustomerID, CompanyName FROM value WHERE COUNTRY='Germany' WITH (SRC='https://services.odata.org/V3/Northwind/Northwind.svc/Customers?$format=json')")
row = cursor.fetchone()
while row:
    print (row[0], row[1])
    row = cursor.fetchone()

