# ====================================
# Get values from Multiple JSON Files
# ====================================
import os
import re
import glob

import pyodbc
from pathlib import Path

# path = "H:/Normade/myProject/WSPythonProjects/python_ws_rest_api"
conn = pyodbc.connect(
    r'DRIVER={ZappySys JSON Driver};'
)
cursor = conn.cursor()

# execute query to fetch data from API service
# cursor.execute("SELECT CustomerID,ContactName FROM value WITH (SRC=@'C:\sql\cust*.json')")
# cursor.execute("SELECT CustomerID,ContactName FROM value WITH (SRC=@'customer2.json')")
# cursor.execute("SELECT CustomerID,ContactName FROM value WITH (SRC=@'\\data\\*')")
#
# row = cursor.fetchone()
# while row:
#     # print two rows
#     print (row[0],row[1])
#     row = cursor.fetchone()

# for filename in os.listdir(path):
#     if re.match("data\\d+.json", filename):
#         print(filename)
#         cursor.execute("SELECT CustomerID,ContactName FROM value WITH (SRC=@'data\\d+.json')")
#         row = cursor.fetchone()
#         while row:
#             # print two rows
#             print(row[0], row[1])
#             row = cursor.fetchone()


# path = "data/*.json"
# for filename in glob.glob(path):
#     print(filename)
#     # GOOD CODE cursor.execute("SELECT CustomerID,ContactName FROM value WITH (SRC='data/customer.json')")
#     cursor.execute("SELECT CustomerID,ContactName FROM value WITH (SRC=filename)")
#     row = cursor.fetchone()
#     while row:
#         # print two rows
#         print(row[0], row[1])
#         row = cursor.fetchone()
#


# List all files in directory using pathlib
basepath = Path('data/')
files_in_basepath = (entry for entry in basepath.iterdir() if entry.is_file())
for item in files_in_basepath:
    print(item.name)
    if item.name == "customer.json":
        cursor.execute("SELECT CustomerID,ContactName FROM value WITH (SRC=@'data/customer.json')")
        row = cursor.fetchone()
        while row:
            # print two rows
            print(row[0], row[1])
            row = cursor.fetchone()
    else:
        cursor.execute("SELECT CustomerID,ContactName FROM value WITH (SRC=@'data/customer2.json')")
        row = cursor.fetchone()
        while row:
            # print two rows
            print(row[0], row[1])
            row = cursor.fetchone()
